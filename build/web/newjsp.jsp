<%-- 
    Document   : newjsp
    Created on : Feb 11, 2016, 3:49:00 PM
    Author     : martin
--%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<html>
<head>
<title>SELECT Operation</title>
</head>
<body>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
 url="jdbc:mysql://localhost/mfl"
 user="root" password="coaches"/>
<sql:query dataSource="${snapshot}" var="result">
SELECT Name,Code,RegNo,KephLevel AS Level,Owner,FacilityType,Owner,RegulatorBody,Beds,Cots,County,Constituency,Ward,
Operation_Status,Open_whole_day,Open_public_holidays,Open_weekends,Open_late_night, 
Services,Categories from mfl_table;
</sql:query>
<table border="1" width="100%">
<tr>
 <th>Name</th>
 <th>Code</th>
 <th>RegNo</th>
 <th>KephLevel</th>
 <th>FacilityType</th>
 <th>Owner</th>
 <th>RegulatorBody</th>
 <th>Beds</th>
 <th>Cots</th>
 <th>County</th>
 <th>Constituency</th>
 <th>Ward</th>
 <th>Operation Status</th>
 <th>Open_whole_day</th>
 <th>Open_public_holidays</th>
 <th>Open_weekends</th>
 <th>Open_late_night</th>
 <th>Services</th>
 <th>Categories</th>
</tr>
<c:forEach var="row" items="${result.rows}">
<tr>
 <td><c:out value="${row.Name}"/></td>
 <td><c:out value="${row.Code}"/></td>
 <td><c:out value="${row.RegNo}"/></td>
 <td><c:out value="${row.Level}"/></td>
 <td><c:out value="${row.FacilityType}"/></td>
 <td><c:out value="${row.Owner}"/></td>
 <td><c:out value="${row.RegulatorBody}"/></td>
 <td><c:out value="${row.Beds}"/></td>
 <td><c:out value="${row.Cots}"/></td>
 <td><c:out value="${row.County}"/></td>
 <td><c:out value="${row.Constituency}"/></td>
 <td><c:out value="${row.Ward}"/></td>
 <td><c:out value="${row.Operation_Status}"/></td>
 <td><c:out value="${row.Open_whole_day}"/></td>
 <td><c:out value="${row.Open_public_holidays}"/></td>
 <td><c:out value="${row.Open_weekends}"/></td>
 <td><c:out value="${row.Open_late_night}"/></td>
 <td><c:out value="${row.Services}"/></td>
 <td><c:out value="${row.Categories}"/></td>
 <td><c:out value="${row.FacilityType}"/></td>
 <td><c:out value="${row.Owner}"/></td>
 <td><c:out value="${row.RegulatorBody}"/></td> 
</tr>
</c:forEach>
</table>
</body>
</html>